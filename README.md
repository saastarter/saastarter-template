# SaaStarter Template

- [Docs](https://docs.saastarter.com)
- [Showcase](https://demo.saastarter.com)

Make sure you go through [Getting started](https://docs.saastarter.com/#/getting-started/quick-start) to set up everything.

## Run Nuxtjs

```bash
npm run dev
```

Template will be available on [http://localhost:3000](http://localhost:3000).

## Run Firebase functions

```bash
cd functions
npm run dev
```

Emulator will be available on [http://localhost:4000](http://localhost:4000).